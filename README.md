# Timer Countdown

## Getting started

Create an HTML block or label in Moodle and paste the code.
Requires bootstrap framework.

## License

https://creativecommons.org/licenses/by/4.0/

## Screenshots

<img width="400" alt="screenshot" src="/screenshots/screenshot.png">
